<?php
/**
 * @file
 * Administration page callbacks for the Menu Item View module.
 */

function _elastic_preview_get_available_entity_types() {
  $entity_types = entity_get_info();
  $available_entity_types = array();

  $excluded_entity_types = array('entityform', 'file');

  foreach ($entity_types as $entity_type => $variables) {
    if (!$variables['fieldable']) {
      continue;
    }

    if (in_array($entity_type, $excluded_entity_types)) {
      continue;
    }

    $available_entity_types[$entity_type] = $variables['label'];
  }

  return $available_entity_types;
}

/**
 * Form builder. Configure Menu Item View
 *
 * @ingroup forms
 * @see system_settings_form().
 */
function elastic_preview_admin_settings() {
  $form = array();

  $form['elastic_preview_entity_types_enabled'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Enabled entity types'),
    '#description' => t('Enabled entity types will have an "Elastic preview" view mode and may have additional options'),
    '#options' => _elastic_preview_get_available_entity_types(),
    '#default_value' => variable_get("elastic_preview_entity_types_enabled", array()),
  );

  $form['elastic_preview_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings'),
    '#description' => t('Configure how Elastic preview items should work'),
    '#collapsible' => false,
    '#collapsed' => false,
  );

  $form['elastic_preview_settings']['elastic_preview_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Elastic preview functionality'),
    '#description' => t('If unchecked, Elastic preview will be disabled site-wide.'),
    '#default_value' => variable_get('elastic_preview_enabled', true),
  );

  $form['elastic_preview_settings']['elastic_preview_static_cache'] = array(
    '#type' => 'checkbox',
    '#title' => t('Cache Elastic preview items as static HTML'),
    '#description' => t('Only takes effect when loading items asynchronously'),
    '#default_value' => variable_get('elastic_preview_static_cache'),
  );

  $form['elastic_preview_settings']['elastic_preview_static_cache_dir'] = array(
    '#type' => 'textfield',
    '#title' => 'Static cache directory',
    '#description' => t('If static cache is enabled, this is the directory under "files" where the cache files will be stored'),
    '#default_value' => variable_get('elastic_preview_static_cache_dir', 'elastic_preview_cache'),
  );

  $form['#submit'][] = 'elastic_preview_admin_settings_submit';

  return system_settings_form($form);
}

function elastic_preview_admin_settings_submit($form, &$form_state) {

}
