(function($) {
  Drupal.behaviors.elasticPreview = {};

  Drupal.behaviors.elasticPreview.attach = function(context, settings) {
    window.ElasticPreview = (function() {
      var grid = $('.elastic-preview-grid'),
        items = grid.find('.elastic-preview-item'),
        current = -1,
        previewPos = -1,
        scrollExtra = 0,
        marginExpanded = 10,
        winSize,
        transEndEventNames = {
          'WebkitTransition' : 'webkitTransitionEnd',
          'MozTransition' : 'transitionend',
          'OTransition' : 'oTransitionEnd',
          'msTransition' : 'MSTransitionEnd',
          'transition' : 'transitionend'
        },
        transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
        support = Modernizr.csstransitions,
        settings = {
          cacheItems: true,
          minHeight: 500,
          speed: 350,
          easing: 'ease',
          onWinSize: null,
          onCalcHeight: null,
          onCreatePreviewEl: null,
          showItem: null,
          onSetHeights: null,
          onCreatePreview: null,
          onSaveItemInfo: null,
          onPositionPreview: null,
          onLoadElasticPreview: null
        };

      function init(config) {
        settings = $.extend(true, {}, settings, config);

        grid.imagesLoaded(function () {
          saveItemInfo(true);

          getWinSize();

          initEvents();
        });
      }

      function addItems(newItems) {
        items = items.add(newItems);

        var index = items.length;

        newItems.each(function () {
          var item = $(this);

          item.data({
            offsetTop: item.offset().top,
            height: item.height(),
            index: index
          });

          index++;
        });
      }

      function saveItemInfo(saveHeight) {
        var index = 0;

        items.each(function () {
          var item = $(this);

          var itemInfo = {
            offsetTop: item.offset().top,
            height: item.height(),
            index: index
          };

          if (settings.onSaveItemInfo !== null) {
            itemInfo = settings.onSaveItemInfo.call(this, itemInfo);
          }

          item.data('offsetTop', itemInfo.offsetTop);

          if (saveHeight) {
            item.data('height', itemInfo.height);
          }

          item.data('index', itemInfo.index);

          index++;
        });
      }

      function initEvents() {
        grid
          .on('click', '.elastic-preview-close', function () {
            hidePreview();

            return false;
          })
          .on('click', '.elastic-preview-previous', function () {
            previousPreview();
          })
          .on('click', '.elastic-preview-next', function () {
            nextPreview();
          })
          .on('click', '.elastic-preview-item > a', function () {
            var item = $(this).parent();

            if (current === item.data('index')) {
              hidePreview();
            } else {
              showPreview(item);
            }

            return false;
          });

        $(window).on('debouncedresize', function () {
          scrollExtra = 0;
          previewPos = -1;

          saveItemInfo();

          getWinSize();

          var preview = $.data(this, 'preview');

          if (typeof preview != 'undefined') {
            hidePreview();
          }
        });
      }

      function getWinSize() {
        winSize = {
          width: $(window).width(),
          height: $(window).height()
        };

        if ($.isFunction(settings.onWinSize)) {
          winSize = settings.onWinSize.call(ElasticPreview, winSize);
        }
      }

      function showPreview(item) {
        var preview = $.data(this, 'preview'),
            position = item.data('offsetTop');

        scrollExtra = 0;

        if (typeof preview != 'undefined') {
          if (previewPos !== position) {
            if (position > previewPos) {
              scrollExtra = preview.height;
            }

            hidePreview();
          } else {
            preview.update(item);

            return false;
          }
        }

        previewPos = position;

        preview = $.data(this, 'preview', new Preview(item));

        preview.open();

        return true;
      }

      function hidePreview() {
        current = -1;

        var preview = $.data(this, 'preview');

        preview.close();

        $.removeData(this, 'preview');
      }

      function previousPreview() {
        if (current > 0) {
          var previousItem = items.eq(current - 1);

          showPreview(previousItem);
        }
      }

      function nextPreview() {
        if (current < (items.length - 1)) {
          var nextItem = items.eq(current + 1);

          showPreview(nextItem);
        }
      }

      function Preview(item) {
        this.item = item;
        this.expandedIdx = this.item.data('index');
        this.create();
        this.update();
      }

      Preview.prototype = {
        create: function () {
          this.prevIcon = $('<i class="fa fa-caret-left"></i>');
          this.nextIcon = $('<i class="fa fa-caret-right"></i>');

          this.loading = $('<div class="elastic-preview-loading"></div>');

          this.closeIcon = $('<i class="fa fa-times"></i>');
          this.closePreview = $('<span class="elastic-preview-close"></span>')
            .append(this.closeIcon);
          this.prevArrow = $('<span class="elastic-preview-previous"></span>')
            .append(this.prevIcon);
          this.nextArrow = $('<span class="elastic-preview-next"></span>')
            .append(this.nextIcon);

          this.previewContent = $('<div class="elastic-preview-content"></div>');

          this.previewInner = $('<div class="elastic-preview-inner"></div>')
            .append(this.loading, this.previewContent, this.closePreview, this.prevArrow, this.nextArrow);

          this.previewEl = $('<div class="elastic-preview-expander"></div>')
            .append(this.previewInner);

          if (settings.onCreatePreviewEl !== null) {
            this.previewEl = settings.onCreatePreviewEl.call(this.previewEl);
          }

          if (settings.showItem === null) {
            this.item.append(this.getEl());
          } else {
            settings.showItem.call(this.item, this.getEl());
          }

          if (settings.onCreatePreview !== null) {
            settings.onCreatePreview(this.previewEl);
          }


          if (support) {
            this.setTransition();
          }
        },

        update: function (item) {
          if (item) {
            this.item = item;
          }

          if (current !== -1) {
            var currentItem = items.eq(current);

            currentItem.removeClass('elastic-preview-expanded');

            this.item.addClass('elastic-preview-expanded');

            this.positionPreview();
          }

          current = this.item.data('index');

          var self = this;

          var itemEl = this.item.children('a'),
            itemUri = itemEl.attr('data-elastic-preview-uri');

          if (typeof(itemUri !== "undefined")) {
            this.loading.show();

            $.ajax({
              url: itemUri,
              dataType: 'html',
              localCache: true,
              cacheTTL: 1,
              cacheKey: itemUri,
              isCacheValid: function () {
                return settings.cacheItems;
              },
              success: function (data) {
                self.loading.hide();

                self.previewContent.html($(data));

                if (settings.onLoadElasticPreview !== null) {
                  settings.onLoadElasticPreview.call(self.previewContent);
                }
              }
            });
          }
        },

        open: function () {
          setTimeout($.proxy(function () {
            this.setHeights();

            this.positionPreview();
          }, this), 25);
        },

        close: function () {
          var self = this,
            onEndFn = function () {
              if (support) {
                $(this).off(transEndEventName);
              }

              self.item.removeClass('elastic-preview-expanded');

              self.previewEl.remove();
            };

          setTimeout($.proxy(function () {
            this.previewEl.css('height', 0);

            var expandedItem = items.eq(this.expandedIdx);

            expandedItem.css('height', expandedItem.data('height')).on(transEndEventName, onEndFn);

            if (!support) {
              onEndFn.call();
            }
          }, this), 25);

          return false;
        },
        calcHeight: function () {
          var heightPreview = winSize.height - this.item.data('height') - marginExpanded,
            itemHeight = winSize.height;

          if (heightPreview < settings.minHeight) {
            heightPreview = settings.minHeight;

            itemHeight = settings.minHeight + this.item.data('height') + marginExpanded;
          }

          if ($.isFunction(settings.onCalcHeight)) {
            var heights = {
              height: heightPreview,
              itemHeight: itemHeight
            };

            heights = settings.onCalcHeight.call(ElasticPreview, heights, this.item.data('height'), marginExpanded);

            this.height = heights.height;
            this.itemHeight = heights.itemHeight;
          } else {
            this.height = heightPreview;
            this.itemHeight = itemHeight;
          }
        },
        setHeights: function () {
          var self = this,
            onEndFn = function () {
              if (support) {
                self.item.off(transEndEventName);
              }

              self.item.addClass('elastic-preview-expanded');
            };

          this.calcHeight();

          this.previewEl.css('height', this.height);

          this.item.css('height', this.itemHeight).on(transEndEventName, onEndFn);

          if (settings.onSetHeights !== null) {
            settings.onSetHeights.call(this);
          }

          if (!support) {
            onEndFn.call();
          }
        },
        positionPreview: function () {
          var position = this.item.data('offsetTop'),
            previewOffsetT = this.previewEl.offset().top - scrollExtra;

          if (settings.onPositionPreview !== null) {
            var data = {
              position: position,
              previewOffsetT: previewOffsetT
            };

            data = settings.onPositionPreview.call(this, data, this.item, settings.speed);

            if (data === false) {
              return;
            }

            position = data.position;
            previewOffsetT = data.previewOffsetT;
          }

          var scrollVal = this.height + this.item.data('height') + marginExpanded <= winSize.height ?
              position :
              this.height < winSize.height ?
                previewOffsetT - (winSize.height - this.height) :
                previewOffsetT;

          $('html, body').animate({scrollTop: scrollVal}, settings.speed);
        },
        setTransition: function () {
          var trans = 'height ' + settings.speed + 'ms ' + settings.easing;

          this.previewEl.css('transition', trans);

          this.item.css('transition', trans);
        },
        getEl: function () {
          return this.previewEl;
        }
      };

      return {
        init: init,
        addItems: addItems
      }
    })();
  };
})(jQuery);

